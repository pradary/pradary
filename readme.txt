Game Design Document:

https://docs.google.com/document/d/1Hanlmp3x_e1Zij9COY24NrLhdIC5HdOcWJ9a1tmg-1Q

Contributors:
Prateek Agarwal
Daniel Zeng
Ryan Chu

Reflection:
We spent over 24 hours together on this game. One of the main obstacles was getting the collisions of each entity correct. Also, another was learning how to use Git and collaborating with other people. 

While we originally planned to implement upgrades and stats, the limited time only allowed us to complete a basic shooting and killing game.