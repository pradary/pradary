import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.Timer;

public class NPC extends Char {
	public static final int EASY = 0;
	public static final int MEDIUM = 1;
	public static final int HARD = 2;

	int diffLevel = 0;
	int delay = 1000;
	Timer t;

	int angle = 0;

	//rework the subclassing
	public NPC(int diffLevel, int x, int y, int tileSize, MBController c) {
		// set difficulty level
		//		setGridPosType(true);//must be before set r c
		super(x, y, 0, "npc", tileSize, true, c);
		this.diffLevel = diffLevel;
		String[] strs = { "npc_back.png", "npc_left.png", "npc_front.png", "npc_right.png" };
		setFileNameMult(strs);
		constructImage();

		// set the delay based on difficulty level
		switch (diffLevel) {
		case EASY:
			delay = 400;
			setSpeed(6);
			break;
		case MEDIUM:
			delay = 270;
			setSpeed(8);
			break;
		case HARD:
			delay = 150;
			setSpeed(10);
			break;
		}

		// make the timer 
		t = new Timer(delay, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				changeDir();
				//				control.updateView();
			}
		});
		t.start();

	}
//	this.diffLevel = diffLevel;
//	setHasFourImages(true);
//	setControl(c);
//	setTileSize(tileSize);
//	setPos(x, y);
//	String[] strs = { "npc_back.png", "npc_left.png", "npc_front.png", "npc_right.png" };
//	setFileNameMult(strs);
//	constructImage();
//	setType("npc");

	public void startT() {
		t.start();
	}

	public void changeDir() {
		int range = 180;
		Random rand = new Random();
		int newA = rand.nextInt(range);
		newA -= range / 2;
		angle = (angle + newA) % 360;
//		System.out.println(angle);
		//		int newR = r;
		//		int newC = c;
		//		switch (dir_move) {
		//		case UP:
		//			newR -= 1;
		//			setDir(UP);
		//			break;
		//		case LEFT:
		//			newC -= 1;
		//			setDir(LEFT);
		//			break;
		//		case DOWN:
		//			newR += 1;
		//			setDir(DOWN);
		//			break;
		//		case RIGHT:
		//			newC += 1;
		//			setDir(RIGHT);
		//			break;
		//		}
		//		if (control.inBounds(newR, newC) && control.isWalk(newR, newC)) {
		//			setPos(newR, newC);
		//		}
	}

	public void move() {
		int newY = getY();
		int newX = getX();
		int oldY = newY;
		int oldX = newX;

		double rad = Math.toRadians(angle);
		int dX = (int) (Math.cos(rad) * speed);
		int dY = (int) (Math.sin(rad) * speed);
		newX += dX;
		newY += dY;
		int tet = angle - 45;
		if (tet < 0)
			tet += 360;
		if (tet == 360)
			tet = 0;
		int image_dir = tet / 90;
		if (image_dir == 0) {
			image_dir = 2;
		} else if (image_dir == 2) {
			image_dir = 0;

		}
		setDir(image_dir);
		//		System.out.println(angle + " " + image_dir);
		//		System.out.println(dX + " " + dY);

		//set the X first
		if (isMoveable(newX, newY)) {
			setPos(newX, newY);
			//			System.out.println(new Point(newX, newY));
		} else if (isMoveable(newX, oldY)) {
			setPos(newX, oldY);
		} else if (isMoveable(oldX, newY)) {
			setPos(oldX, newY);
		}
//		System.out.println(x + " " + y);

	}
	//	ic void move() {
	//		int newY = getY();
	//		int newX = getX();
	//		int oldY = newY;
	//		int oldX = newX;
	//		
	//		switch (getDir()) {
	//		case UP:
	//			newY -= getSpeed();
	//			break;
	//		case LEFT:
	//			newX -= getSpeed();
	//			break;
	//		case DOWN:
	//			newY += getSpeed();
	//			break;
	//		case RIGHT:
	//			newX += getSpeed();
	//			break;
	//		}
	//		//set the X first
	//		if (isMoveable(newX, newY)) {
	//			setPos(newX, newY);
	//			//			System.out.println(new Point(newX, newY));
	//		} else if (isMoveable(newX, oldY)) {
	//			setPos(newX, oldY);
	//		} else if (isMoveable(oldX, newY)) {
	//			setPos(oldX, newY);
	//		}
	//		
	//	}

	public int getDiffLevel() {
		return diffLevel;
	}

	public void setDiffLevel(int diffLevel) {
		this.diffLevel = diffLevel;
	}

}
