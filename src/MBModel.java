import java.awt.Image;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

public class MBModel {
	MBController control = null;
	BackgroundTile[][] map;
	Player p;
	NPC ai_1;

	int rows = 10;
	int cols = 10;
	int tileSize;

	public static final int UP = 0;
	public static final int LEFT = 1;
	public static final int DOWN = 2;
	public static final int RIGHT = 3;

	int mode = 0;
	public static final int TITLE = 0;
	public static final int GAME = 1;
	public static final int MENU = 2;
	public static final int LOADING = 3;

	boolean useWidth;
	int size = 0;

	ArrayList<Char> chars = new ArrayList<Char>();
	ArrayList<Bullet> bullets = new ArrayList<Bullet>();
	ArrayList<Point> walkables = new ArrayList<Point>();
	
	int bulletBaseDamage = 10;
	
	Random ran = new Random();
	
	//right now fix tile size, and make it so that the NPC walks around in smooth motion
	public MBModel(MBController c, int size, boolean useWidth, BackgroundTile[][] map) {
		this.useWidth = useWidth;
		this.size = size;
		setMap(map);

		this.control = c;
		mode = TITLE; //starting game

		//below is broken right now because it can't move cuz it's right next to a notWalkable Tile
		//EDIT: FIXED

		Point initPoint = walkables.get(ran.nextInt(5));
		p = new Player((int) initPoint.getX(), (int) initPoint.getY(), tileSize, c);
		//		p.setPos(tileSize * 10, tileSize * 5);
		//		p.setModel(this);
		//overritten by later on by a function
		//		p.setSpeed(10);

		//AI
		//cgp.setFileName();    Make another sprite picture for a random bad guy on the board
//		NPC ai_1 = new NPC(0, 1 * tileSize, 6 * tileSize, tileSize, c);
//		NPC ai_2 = new NPC(2, 3 * tileSize, 6 * tileSize, tileSize, c);

		chars.add(p);
//		chars.add(ai_1);
//		chars.add(ai_2);
		
	}

	public BackgroundTile[][] getMap() {
		return map;
	}
	
	public void newNPC(int diff){
		Point p = walkables.get(ran.nextInt(walkables.size()));
		chars.add(new NPC(diff, (int) p.getX(), (int) p.getY(), tileSize, control));
	}
	
	public void setMap(BackgroundTile[][] map) {
		this.map = map;
		cols = map[0].length;
		rows = map.length;
		if (useWidth) {
			//filling the width
			tileSize = size / cols;
		} else {
			//filling the height
			tileSize = size / rows;
		}

		//		calcTileSize();
		//		System.out.println(tileSize);
		//dont store data in player, rather have it get it from model
		//		p.setTileSize(tileSize);
		//		p.setRows(rows);
		//		p.setCols(cols);

		//finds the closest walkable tile
//		initPoint = setInitialPosition();
		//		p.setPos((int) initPoint.getX(), (int) initPoint.getY());
		
		setWalkables();
	}

	private void setWalkables() {
		walkables.clear();
		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < cols; c++) {
				if (isWalk(r, c, false)) {
					walkables.add(new Point(c * tileSize, r * tileSize));
				}
			}
		}
	}

	//	private void calcTileSize() {
	//		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	//		double winL = screenSize.getWidth();
	//		double winH = screenSize.getHeight();
	//		if(winL < winH){
	////			tileSize = (int)(winL/map[0].length);
	//			tileSize = (int) winL/20;
	//		} else {
	//			tileSize = (int)winH/20;
	////			tileSize = (int)(winH/map.length);
	//		}
	//	}

	

	//	public void updatePlayerPos(Player player, int r, int c) {
	//		player.setPos(r, c);
	//	}

	//	public int getPlayerR() {
	//		return p.getR();
	//	}
	//
	//	public int getPlayerC() {
	//		return p.getC();
	//	}

	public boolean isWalk(int r, int c, boolean isPlayer) {
		BackgroundTile bt = map[r][c];
		String f = bt.getFileName();
		if(isPlayer && mode == GAME && f.equals("BottomDoor.png")){
//			System.out.println("change");
			mode = LOADING;
			control.changeMap();
			mode = GAME;
//			p.setPos(0, 0);
//			return false;
		}
		return bt.getPropertyWalkable();
		
	}

	public void shoot(int x, int y, double angle) {
		Bullet bullet = new Bullet(x, y, angle, bulletBaseDamage, tileSize, control);
		bullets.add(bullet);
	}

	//	private void calcTileSize() {
	//		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	//		double winL = screenSize.getWidth();
	//		double winH = screenSize.getHeight();
	//		if(winL < winH){
	////			tileSize = (int)(winL/map[0].length);
	//			tileSize = (int) winL/20;
	//		} else {
	//			tileSize = (int)winH/20;
	////			tileSize = (int)(winH/map.length);
	//		}
	//	}
	
	public boolean inBounds(int ro, int co) {
		if (ro < rows && ro >= 0) {
			if (co < cols && co >= 0) {
				return true;
			}
		}
		return false;
	}

	public int getMode() {
		return mode;
	}

	public int getRows() {
		return rows;
	}

	public int getCols() {
		return cols;
	}

	public int getTileSize() {
		return tileSize;
	}

	public BackgroundTile getBT(int r, int c) {
		return map[r][c];
	}

	public Image getBTImage(int r, int c) {
		return map[r][c].getI();
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public Player getPlayer() {
		return p;
	}

	public ArrayList<Char> getChars() {
		return chars;
	}
	public void clearNPCs() {
		chars.subList(1, chars.size()).clear();
	}

	public ArrayList<Bullet> getBullets() {
		return bullets;
	}
}
