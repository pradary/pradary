import java.util.ArrayList;

public class Char extends Entity {
	int maxHP = 100;
	int health = maxHP;
	int cash = 0;
	double regen = 0;
	private ArrayList<Item> items;

	//	public Char(int x, int y) {
	//		setPos(x, y);
	//		setHasFourImages(true);
	//		String[] strs = {"player_back.png", "player_left.png", "player_front.png", "player_right.png"};
	//		setFileNameMult(strs);
	//		constructImage();
	//		setType("player");
	//		setSpeed(10);
	//	}
	public Char(int x, int y, int speed, String type, int tileSize, boolean hasFourImages, MBController c) {
		super(speed, type, tileSize, hasFourImages, c);
		setPos(x, y);
	}

	// Getters and Setters

	// checks if the item is moveable. x and y are the top left corner
	public boolean isMoveable(int x, int y) {
		//why it never reaches the border: cuz the next move is out of bounds, so it doesn't move
		updateTileBounds(x, y);
		// check if it's corners are touching an unwalkable object
		// first, covert the corner to row/col
		//you have to floor it to round to the nearest row/col
		int c_base = (int) Math.floor((tile_bounds.getX()) / tileSize);
		int c_top = (int) Math.floor((tile_bounds.getX() + tile_bounds.getWidth()) / tileSize);
		int r_base = (int) Math.floor((tile_bounds.getY()) / tileSize);
		int r_top = (int) Math.floor((tile_bounds.getY() + tile_bounds.getHeight()) / tileSize);
		//			System.out.println(c_base + " " + r_base + " " + c_top + " " + r_top);
		
		if (c_top < control.getCols() && c_base >= 0 && r_top < control.getRows() && r_base >= 0) {
			// it's within the bounds of the screen/ game board
//			if(type.equals("player")){
//				System.out.println(c_base + " " + r_base + " " + tile_bounds.getX());
//			}
		} else {
			return false;
		}
		boolean checkDoor = type.equals("player");
//		if(checkDoor) System.out.println(x);
		// top left corner
		
		return control.isWalk(r_base, c_base, checkDoor) && control.isWalk(r_base, c_top, checkDoor) && control.isWalk(r_top, c_base, checkDoor)
				&& control.isWalk(r_top, c_top, checkDoor);
		// if it's at this stage, it passed all the tests. It is moveable.
		//			return true;
	}

	// Getters and Setters
	
	@Override
	public void setPos(int x, int y) {
		super.setPos(x, y);
		updateCollisionBox(x, y);
		updateTileBounds(x, y);
		//		System.out.println(dx + " " + dy + " " + tileSize);
	
	}

	public void updateCollisionBox(int x, int y) {
		double xStart = 0.25;
		double xEnd = 0.75;
		double yStart = 0;
		double yEnd = 1;
		double nx = ((double) x) + (double) (tileSize) * xStart;
		double ny = ((double) y) + (double) (tileSize) * yStart;
		double dx = (double) (tileSize) * (xEnd - xStart);
		double dy = (double) (tileSize) * (yEnd - yStart);
		collision.setRect(nx, ny, dx, dy);
	}

	public void updateTileBounds(int x, int y) {
		double xStart = .3;
		double xEnd = 0.7;
		double yStart = .5;
		double yEnd = .8;
		double nx = ((double) x) + (double) (tileSize) * xStart;
		double ny = ((double) y) + (double) (tileSize) * yStart;
		double dx = (double) (tileSize) * (xEnd - xStart);
		double dy = (double) (tileSize) * (yEnd - yStart);
		tile_bounds.setRect(nx, ny, dx, dy);
//		System.out.println(dx + " " + dy + " " + tileSize);
	}

	public int getMaxHP() {
		return maxHP;
	}

	public void setMaxHP(int maxHP) {
		this.maxHP = maxHP;
	}

	public int getCash() {
		return cash;
	}

	public void setCash(int cash) {
		this.cash = cash;
	}

	public ArrayList<Item> getItems() {
		return items;
	}

	public void setItems(ArrayList<Item> items) {
		this.items = items;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}
	public void changeHealth(int c) {
		health += c;
	}

	//	public void setTileSize(int tileSize) {
	//		this.tileSize = tileSize;
	//	}
	//
	//	public void setRows(int rows) {
	//		this.rows = rows;
	//	}
	//
	//	public void setCols(int cols) {
	//		this.cols = cols;
	//	}

}
