public class Bullet extends Entity {
	int damage;
	double angle = 0;

	//add the images for filetypes

	//int x, int y, int speed, String type, int tileSize, boolean hasFourImages, MBController c
	public Bullet(int x, int y, double angle, int damage, int tileSize, MBController c) {
		super(20, "bullet", tileSize, true, c);
		setPos(x, y);
		this.damage = damage;
		this.angle = angle;
		String[] strs = { "bullet_up.png", "bullet_left.png", "bullet_down.png", "bullet_right.png" };
		setFileNameMult(strs);
		constructImage();
	}
	//		setPos(x, y);
	//		this.damage = damage;
	//		this.angle = angle;
	//		setHasFourImages(true);
	//		setTileSize(tileSize);
	//		String[] strs = {"bullet_up.png", "bullet_left.png", "bullet_down.png", "bullet_right.png"};
	//		setFileNameMult(strs);
	//		constructImage();
	//		setType("bullet");
	//		setSpeed(20);

	public boolean isMoveable(int x, int y) {
		//why it never reaches the border: cuz the next move is out of bounds, so it doesn't move
		updateCollisionBox(x, y);
		// check if it's corners are touching an unwalkable object
		// first, covert the corner to row/col
		//you have to floor it to round to the nearest row/col
		int c_base = (int) Math.floor((collision.getX()) / tileSize);
		int c_top = (int) Math.floor((collision.getX() + collision.getWidth()) / tileSize);
		int r_base = (int) Math.floor((collision.getY()) / tileSize);
		int r_top = (int) Math.floor((collision.getY() + collision.getHeight()) / tileSize);
		//			System.out.println(c_base + " " + r_base + " " + c_top + " " + r_top);

		if (c_top < control.getCols() && c_base >= 0 && r_top < control.getRows() && r_base >= 0) {
			// it's within the bounds of the screen/ game board
			//			if(type.equals("player")){
			//				System.out.println(c_base + " " + r_base + " " + tile_bounds.getX());
			//			}
		} else {
			return false;
		}

		// top left corner
		return control.isWalk(r_base, c_base, false) && control.isWalk(r_base, c_top, false) && control.isWalk(r_top, c_base, false) && control.isWalk(r_top, c_top, false);

		// if it's at this stage, it passed all the tests. It is moveable.
		//			return true;
	}

	@Override
	public void setPos(int x, int y) {
		super.setPos(x, y);
		updateCollisionBox(x, y);
	}
//	double xStart = 0;
//	double xEnd = 0.3;
//	double yStart = 0;
//	double yEnd = 0.4;
	//update collision box for based on orientation?
	public void updateCollisionBox(int x, int y) {
		double xStart = 0;
		double xEnd = 0.4;
		double yStart = 0;
		double yEnd = 0.4;
		double nx = ((double) x) + (double) (tileSize) * xStart;
		double ny = ((double) y) + (double) (tileSize) * yStart;
		double dx = (double) (tileSize) * (xEnd - xStart);
		double dy = (double) (tileSize) * (yEnd - yStart);
		collision.setRect(nx, ny, dx, dy);
	}

	public boolean move() {
		//		System.out.println(angle);
		int newY = getY();
		int newX = getX();

		int dX = (int) (Math.cos(angle) * speed);
		int dY = (int) (Math.sin(angle) * speed);
		double deg = Math.toDegrees(angle);
		newX += dX;
		newY += dY;
		double tet = deg - 45;
		if (tet < 0)
			tet += 360;
		if (tet == 360)
			tet = 0;
		int image_dir = (int) (tet) / 90;
		if (image_dir == 0) {
			image_dir = 2;
		} else if (image_dir == 2) {
			image_dir = 0;
		}
//		System.out.println(image_dir + " " + tet);
		setDir(image_dir);
		if (isMoveable(newX, newY)) {
			setPos(newX, newY);
			return false;
		} else {
			control.getBullets().remove(this);
			return true;
		}

	}

	//	public Bullet(int x, int y, int damage, int angle, int tileSize, MBController c) {
	//		setPos(x, y);
	//		this.damage = damage;
	//		this.angle = angle;
	//		setHasFourImages(true);
	//		setTileSize(tileSize);
	//		String[] strs = {"bullet_up.png", "bullet_left.png", "bullet_down.png", "bullet_right.png"};
	//		setFileNameMult(strs);
	//		constructImage();
	//		setType("bullet");
	//		setSpeed(20);
	//	}

	public int getDamage() {
		return damage;
	}

	public double getAngle() {
		return angle;
	}

}
