import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Random;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

public class MBView {

	public static final int UP = 0;
	public static final int LEFT = 1;
	public static final int DOWN = 2;
	public static final int RIGHT = 3;

	public static final int TITLE = 0;
	public static final int GAME = 1;
	public static final int MENU = 2;
	public static final int EMPTY = 3;

	MBController control = null;
	ViewingPanel view;
	VidPlayer vid = new VidPlayer();

	JFrame window;
	JPanel title;

	/**
	 * Menu Items
	 */
	InGameMenu menu;
	JButton items;
	JButton status;
	JButton save;
	JButton load;
	JButton quit;

	Timer visual_t, mech_t;
	TimerTask refreshFPS;

	double refresh = 60; //fps cap
	double fps = 0;
	int frames = 0;

	int pic_length, winL, winH, size;
	//	public int size;

	//mouse location
	int mX, mY;
	boolean shoot = false;
	int shootCount = 0;

	Random ran = new Random();

	boolean[] keys = new boolean[4];

	int secs = 0;
	int stopSecs = 30; 

	public MBView(MBController c, int size, int winL, int winH) {
		this.control = c;
		this.winH = winH;
		this.winL = winL;
		this.size = size;

		/**
		 * Get monitor size for fullscreen
		 */
		//		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		//		winL = screenSize.getWidth();
		//		winH = screenSize.getHeight();

		window = new JFrame("Mob Tycoon");
		window.setBounds(0, 0, winL, winH);
		window.setResizable(false);
		window.setUndecorated(true);
		//window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		window.setLayout(null);

		window.addMouseListener(new DownListen());
		window.addMouseMotionListener(new DragListen());

		/**
		 * Prepare sizes for the view
		 * 
		 * @size is the smallest dimension of the monitor - either width or
		 *       height
		 */
		//		size = (int) (smallestDimension());
		pic_length = c.getTileSize();
		//		System.out.println(pic_length);

		/**
		 * Menu
		 */
		menu = new InGameMenu();
		menu.setBounds(winL - size / 2 - size / 8, winH - size / 2 + size / 8, size / 4, size / 20);
		menu.setUndecorated(true);
		menu.setLayout(new FlowLayout());

		menu.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyTyped(KeyEvent e) {
				if (e.getKeyChar() == 'e') {
					menu.setVisible(false);
					System.out.println("HELELELEL");
				}
			}

		});

		JLabel menuLabel = new JLabel();
		menuLabel.setText("Menu");
		menu.add(menuLabel);
		/**
		 * Not DONE
		 */
		items = new JButton("Items");
		status = new JButton("Status");
		save = new JButton("Save");
		load = new JButton("Load");

		quit = new JButton("Quit");
		quit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}

		});
		menu.add(items);
		menu.add(status);
		menu.add(save);
		menu.add(load);
		menu.add(quit);

		//key strokes
		window.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {

			}

			@Override
			public void keyReleased(KeyEvent e) {
				int k = e.getKeyCode();
				switch (control.getMode()) {
				case GAME:
					//					keys[UP] = !(k == KeyEvent.VK_W || k == KeyEvent.VK_UP) && keys[UP];
					//					keys[LEFT] = !(k == KeyEvent.VK_A || k == KeyEvent.VK_LEFT) && keys[LEFT];
					//					keys[DOWN] = !(k == KeyEvent.VK_S || k == KeyEvent.VK_DOWN) && keys[DOWN];
					//					keys[RIGHT] = !(k == KeyEvent.VK_D || k == KeyEvent.VK_RIGHT) && keys[RIGHT];
					if (k == KeyEvent.VK_W || k == KeyEvent.VK_UP) {
						//up
						//											System.out.println("released up");
						keys[UP] = false;
					}
					if (k == KeyEvent.VK_A || k == KeyEvent.VK_LEFT) {
						//						left
						//											System.out.println("released left");
						keys[LEFT] = false;

					}
					if (k == KeyEvent.VK_S || k == KeyEvent.VK_DOWN) {
						//down
						//											System.out.println("released down");
						keys[DOWN] = false;

					}
					if (k == KeyEvent.VK_D || k == KeyEvent.VK_RIGHT) {
						//right
						//											System.out.println("released right");
						keys[RIGHT] = false;

					}
					break;
				}

			}

			@Override
			public void keyPressed(KeyEvent e) {
				//				System.out.println("before" + e.getKeyCode());
				int k = e.getKeyCode();
				switch (control.getMode()) {
				case GAME:
					//WASD and Arrowkeys
					//somehow using if works and using the = doesnt work
					//					keys[UP] = (k == KeyEvent.VK_W || k == KeyEvent.VK_UP) && !keys[UP];
					//					keys[LEFT] = (k == KeyEvent.VK_A || k == KeyEvent.VK_LEFT) && !keys[LEFT];
					//					keys[DOWN] = (k == KeyEvent.VK_S || k == KeyEvent.VK_DOWN) && !keys[DOWN];
					//					keys[RIGHT] = (k == KeyEvent.VK_D || k == KeyEvent.VK_RIGHT && !keys[RIGHT]);
					if (k == KeyEvent.VK_W || k == KeyEvent.VK_UP) {
						//up
						//											System.out.println("pressed up");
						keys[UP] = true;
					}
					if (k == KeyEvent.VK_A || k == KeyEvent.VK_LEFT) {
						//left
						//											System.out.println("pressed left");
						keys[LEFT] = true;

					}
					if (k == KeyEvent.VK_S || k == KeyEvent.VK_DOWN) {
						//down
						//											System.out.println("pressed down");
						keys[DOWN] = true;

					}
					if (k == KeyEvent.VK_D || k == KeyEvent.VK_RIGHT) {
						//right
						//											System.out.println("pressed right");
						keys[RIGHT] = true;

					}
					//					System.out.println("after" + e.getKeyCode());

					if (k == KeyEvent.VK_E) {
						//not done
						if (menu.isVisible())
							menu.setVisible(false);
						else
							menu.setVisible(true);
					}
					break;

				case TITLE:
					if (k == KeyEvent.VK_ENTER) {
						control.startGame();
					}
					break;
				}

			}
		});

		/**
		 * Title Screen
		 */
		title = new JPanel();
		JLabel titleAnimation = new JLabel();
		title.setBounds(0, 0, size, size);
		title.setBackground(Color.BLACK);
		title.add(titleAnimation);
		window.add(title);
		vid.playVid("title.gif", titleAnimation, window);
		String show = "You are the mob boss. \n" + "Kill as many enemies in " + stopSecs + " seconds.\n"
				+ "Show the world who's the best mob boss.";
		JOptionPane.showMessageDialog(null, show);
		//testing:
		//		control.startGame();

		window.setVisible(true);

		//weird, somehow always gets capped at ~33FPS

		//visual update timer
		visual_t = new Timer((int) (1000 / refresh), new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				update();
				frames++;
			}
		});
		//movement and other stuff update
		int mechUpdateRate = 60;
		int npcChance = 150;
		int shootRate = 10;
		mech_t = new Timer((int) (1000 / mechUpdateRate), new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.moveAll(keys);
				if (menu.isVisible()) {
					menu.requestFocus();
				}
				if (shoot) {
					shootCount++;
					if (shootCount % shootRate == 0) {
						control.shoot(mX, mY);
					}
				}
				if (ran.nextInt(npcChance) == 0) {
					control.newNPC();
				}
				//				System.out.println(shootCount);
				control.checkCollisions();
			}
		});
		refreshFPS = new TimerTask() {
			@Override
			public void run() {
				fps = frames;
				frames = 0;
				secs++;
				if (secs > stopSecs) {
					mech_t.stop();
					int enemies = control.getEnemiesKilled();
					String enemy_end = "";
					if(enemies == 1){
						enemy_end = "enemy";
					}else{
						enemy_end = "enemies";
					}
					String rank = "";
					int inc = stopSecs/6;
					if(enemies < inc * 1){
						rank = "Noob";
					}else if(enemies < inc * 2){
						rank = "Novice";
					}else if(enemies < inc * 3){
						rank = "Expert";
					}else if(enemies < inc * 4){
						rank = "Master";
					}else if(enemies >= inc * 4){
						rank = "GRAND Master";
					}
					String show = "You have killed " + enemies + " " + enemy_end + ".\n"
							+ "You are a: " + rank + " Mob Boss\n"
									+ "Restart the game to try again";
					JOptionPane.showMessageDialog(null, show);
					this.cancel();
				}
				//				System.out.println(fps + " FPS");
			}
		};
		java.util.Timer fpsRun = new java.util.Timer();
		fpsRun.scheduleAtFixedRate(refreshFPS, 1000, 1000);

	}

	/**
	 * Implementing the Drawing Panel / Starting the Game
	 */
	public void startGame() {
		title.setVisible(false);
		control.setMode(GAME);
		view = new ViewingPanel();
		view.setBounds(0, 0, winL, winH);
		window.add(view);
		view.repaint();
		visual_t.start();
		mech_t.start();
	}

	public void update() {
		view.repaint();
	}

	//	private double smallestDimension() {
	//		if (winL < winH) {
	//			return winH;
	//		}
	//		return winL;
	//	}

	//	private class ClickListen extends MouseAdapter {
	//
	//		@Override
	//		public void mouseClicked(MouseEvent e) {
	//			switch (control.getMode()) {
	//			case GAME:
	//				if (SwingUtilities.isLeftMouseButton(e)) {
	//					control.bullet( e.getX(), e.getY() );
	//					//				System.out.println(e.getX() + " " + e.getY());
	//				} else if (SwingUtilities.isRightMouseButton(e)) {
	//				}
	//				//				update();
	//				break;
	//
	//			case TITLE:
	//				if (SwingUtilities.isLeftMouseButton(e)) {
	//					control.startGame();
	//				}
	//				break;
	//			}
	//
	//		}
	//
	//	}
	private class DownListen implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseEntered(MouseEvent e) {

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent e) {
			switch (control.getMode()) {
			case GAME:
				if (SwingUtilities.isLeftMouseButton(e)) {
					mX = e.getX();
					mY = e.getY();
					shoot = true;
				} else if (SwingUtilities.isRightMouseButton(e)) {
				}
				//				update();
				break;

			case TITLE:
				if (SwingUtilities.isLeftMouseButton(e)) {
					control.startGame();
				}
				break;
			}
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			switch (control.getMode()) {
			case GAME:
				if (SwingUtilities.isLeftMouseButton(e)) {
					shoot = false;
				} else if (SwingUtilities.isRightMouseButton(e)) {
				}
				break;
			}
		}

	}

	private class DragListen extends MouseAdapter {
		@Override
		public void mouseDragged(MouseEvent e) {
			switch (control.getMode()) {
			case GAME:
				if (SwingUtilities.isLeftMouseButton(e)) {
					mX = e.getX();
					mY = e.getY();
				} else if (SwingUtilities.isRightMouseButton(e)) {
				}
				//				update();
				break;

			case TITLE:
				if (SwingUtilities.isLeftMouseButton(e)) {
					control.startGame();
				}
				break;
			}
		}

	}

	private class ViewingPanel extends JPanel {
		String dir = "images/";

		static final long serialVersionUID = 1234567890L;

		public void paintComponent(Graphics g) {
			g.setColor(Color.black);
			g.fillRect(0, 0, size, size);

			/**
			 * Health bars, etc.
			 */
			g.setColor(Color.RED);
			g.fillRect(size / 60, size / 2 + size / 60, size / 2, size / 40);
			g.setColor(Color.WHITE);
			g.drawString("HP", size / 60, size / 2 + size / 60);

			//draws the tiles
			int rows = control.getRows();
			int cols = control.getCols();
			//				System.out.println(rows + " " + cols);
			for (int r = 0; r < rows; r++) {
				for (int c = 0; c < cols; c++) {
					//						System.out.println(control.getTileImage( y, x ));
					//						Image currTile = ImageIO.read(new File(dir + control.getTileImage(r, c)));
					g.drawImage(control.getBTImage(r, c), c * pic_length, r * pic_length, pic_length, pic_length, null);
				}
			}
			g.setColor(Color.red);
			boolean drawBoundingBox = false;
			boolean drawSubBoundingBox = false;
			boolean drawCollisionBox = false;

			//draws the entities
			ArrayList<Char> chars = control.getChars();
			for (Char c : chars) {
				int x = c.getX();
				int y = c.getY();

				g.drawImage(c.getI(c.getDir()), x, y, pic_length, pic_length, null);

				if (drawBoundingBox) {
					g.drawLine(x, y, x + pic_length, y);
					g.drawLine(x, y, x, y + pic_length);
					g.drawLine(x, y + pic_length, x + pic_length, y + pic_length);
					g.drawLine(x + pic_length, y, x + pic_length, y + pic_length);
				}
				if (drawSubBoundingBox) {
					int r_base = (int) (y + pic_length * .5);
					int c_base = (int) (x + pic_length * .3);
					int r_top = (int) (y + pic_length * .8);
					int c_top = (int) (x + pic_length * .7);
					g.drawLine(c_base, r_base, c_base, r_top);
					g.drawLine(c_base, r_base, c_top, r_base);
					g.drawLine(c_base, r_top, c_top, r_top);
					g.drawLine(c_top, r_base, c_top, r_top);
				}
				if (drawCollisionBox) {
					g.setColor(Color.green);
					Rectangle2D.Double a = c.getCollision();
					int c_base = (int) (a.getX());
					int r_base = (int) (a.getY());
					int c_top = (int) (a.getX() + a.getWidth());
					int r_top = (int) (a.getY() + a.getHeight());
					//					System.out.println(c_base + " " + r_base + " " + c_top  + " " + r_top);
					g.drawLine(c_base, r_base, c_base, r_top);
					g.drawLine(c_base, r_base, c_top, r_base);
					g.drawLine(c_base, r_top, c_top, r_top);
					g.drawLine(c_top, r_base, c_top, r_top);
				}

				//				if (ent.getType().equals("npc")) {
				//					String a = ent.getType();
				//					System.out.println(a + " " + r + " " + c);
				//				} else {
				//					g.drawImage(ent.getI(ent.getDir()), ent.getX(), ent.getY(), pic_length, pic_length, null);
				//				}
			}
			g.setColor(Color.red);
			ArrayList<Bullet> buls = control.getBullets();
			//figure out how to rotate the bullets based on angle
			for (Bullet c : buls) {
				int x = c.getX();
				int y = c.getY();

				g.drawImage(c.getI(c.getDir()), x, y, pic_length, pic_length, null);

				if (drawBoundingBox) {
					g.drawLine(x, y, x + pic_length, y);
					g.drawLine(x, y, x, y + pic_length);
					g.drawLine(x, y + pic_length, x + pic_length, y + pic_length);
					g.drawLine(x + pic_length, y, x + pic_length, y + pic_length);
				}
				if (drawSubBoundingBox) {
					int r_base = (int) (y + pic_length * .5);
					int c_base = (int) (x + pic_length * .3);
					int r_top = (int) (y + pic_length * .8);
					int c_top = (int) (x + pic_length * .7);
					g.drawLine(c_base, r_base, c_base, r_top);
					g.drawLine(c_base, r_base, c_top, r_base);
					g.drawLine(c_base, r_top, c_top, r_top);
					g.drawLine(c_top, r_base, c_top, r_top);
				}
				if (drawCollisionBox) {
					g.setColor(Color.green);
					Rectangle2D.Double a = c.getCollision();
					int c_base = (int) (a.getX());
					int r_base = (int) (a.getY());
					int c_top = (int) (a.getX() + a.getWidth());
					int r_top = (int) (a.getY() + a.getHeight());
					//					System.out.println(c_base + " " + r_base + " " + c_top  + " " + r_top);
					g.drawLine(c_base, r_base, c_base, r_top);
					g.drawLine(c_base, r_base, c_top, r_base);
					g.drawLine(c_base, r_top, c_top, r_top);
					g.drawLine(c_top, r_base, c_top, r_top);
				}
			}
			boolean drawLines = false;
			g.setColor(Color.lightGray);
			if (drawLines) {
				for (int c = 0; c < this.getWidth(); c += pic_length)
					g.drawLine(c, 0, c, this.getHeight());

				for (int r = 0; r < this.getHeight(); r += pic_length)
					g.drawLine(0, r, this.getWidth(), r);

			}

		}

	}

	private class VidPlayer {

		public void playVid(String vid, JLabel l, JFrame f) {
			ImageIcon icon = new ImageIcon(vid);
			l.setIcon(icon);
			f.repaint();
			//audio();
		}
	}

	private class InGameMenu extends JDialog {

		private static final long serialVersionUID = 1L;

		public void paintComponent(Graphics g) {
			g.setColor(Color.BLACK);
			g.drawString("Menu", 20, 20);
		}

	}

	private class TextBox extends JDialog {

		private static final long serialVersionUID = 1L;

	}

}

////int dir = 0;
//if (k == KeyEvent.VK_W || k == KeyEvent.VK_UP) {
//	//up
////						System.out.println("up");
//	keys[UP] = true;
//}
//if (k == KeyEvent.VK_A || k == KeyEvent.VK_LEFT) {
//	//left
//	//					System.out.println("left");
//	keys[LEFT] = true;
//
//}
//if (k == KeyEvent.VK_S || k == KeyEvent.VK_DOWN) {
//	//down
//	//					System.out.println("down");
//	keys[DOWN] = true;
//
//}
//if (k == KeyEvent.VK_D || k == KeyEvent.VK_RIGHT) {
//	//right
//	//					System.out.println("right");
//	keys[RIGHT] = true;
//
//}
//g.setColor(Color.lightGray);
//			for (int c = 0; c < this.getWidth(); c += pic_length)
//				g.drawLine(c, 0, c, this.getHeight());
//
//			for (int r = 0; r < this.getHeight(); r += pic_length)
//				g.drawLine(0, r, this.getWidth(), r);
