import java.awt.Image;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Entity {
	String[] fileName = { "", "", "", "" };
	String dir_f = "images/";
	boolean isGridPosType;
	int x, y, speed, tileSize;
	//	int r, c;
	String type = "";
	Image[] i = new Image[4];
	int dir = 2;
	boolean hasFourImages = false;

	public static final int UP = 0;
	public static final int LEFT = 1;
	public static final int DOWN = 2;
	public static final int RIGHT = 3;

	MBController control = null;

	Rectangle2D.Double collision = new Rectangle2D.Double();
	Rectangle2D.Double tile_bounds = new Rectangle2D.Double();

	public Entity(int speed, String type, int tileSize, boolean hasFourImages, MBController c) {
		this.speed = speed;
		this.type = type;
		this.tileSize = tileSize;
		this.hasFourImages = hasFourImages;
		this.control = c;
	}

	public void setTileSize(int tileSize) {
		this.tileSize = tileSize;
	}

	public void setPos(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Rectangle2D.Double getCollision() {
		return collision;
	}

	// checks if the item is moveable. x and y are the top left corner
	public boolean isMoveable(int x, int y) {
		// right now, this assumes that the character size is the full tile size.
		// once the final sprites are made, use the actual size of the characters to
		// determine if a bullet hits them or not. 

		// check if it's in the screen
		int width = control.getCols() * control.getTileSize();
		int height = control.getRows() * control.getTileSize();
		if (x + tileSize < width && x >= 0 && y + tileSize < height && y >= 0) {
			// it's within the bounds of the screen/ game board
		} else {
			return false;
		}
		//why it never reaches the border: cuz the next move is out of bounds, so it doesn't move

		// check if it's corners are touching an unwalkable object
		// first, covert the corner to row/col
		int r_base = (int) ((y + tileSize * .5) / tileSize);
		int c_base = (int) ((x + tileSize * .3) / tileSize);
		int r_top = (int) ((y + tileSize * .8) / tileSize);
		int c_top = (int) ((x + tileSize * .7) / tileSize);

		// top left corner
		return control.isWalk(r_base, c_base, false) && control.isWalk(r_base, c_top, false)
				&& control.isWalk(r_top, c_base, false) && control.isWalk(r_top, c_top, false);
				//		// top right corner
				//		if (!control.isWalk((int) ((double) y / tileSize), (int) ((double) (x + tileSize) / tileSize))) {
				//			return false;
				//		}
				//		// bottom left corner
				//		if (!control.isWalk((int) ((double) (y + tileSize) / tileSize), (int) ((double) x / tileSize))) {
				//			return false;
				//		}
				//		// bottom right corner
				//		if (!control.isWalk((int) ((double) (y + tileSize) / tileSize), (int) ((double) (x + tileSize) / tileSize))) {
				//			return false;
				//		}

		// if it's at this stage, it passed all the tests. It is moveable.
		//		return true;
	}

	//	public int getR() {
	//		return r;
	//	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}
	//	
	//	public int getC() {
	//		return c;
	//	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public boolean isHasFourImages() {
		return hasFourImages;
	}

	public int getDir() {
		return dir;
	}

	public void setDir(int dir) {
		this.dir = dir;
	}

	public void setHasFourImages(boolean hasFourImages) {
		this.hasFourImages = hasFourImages;
	}

	public void constructImage() {
		try {
			if (hasFourImages) {
				for (int in = 0; in < fileName.length; in++) {
					i[in] = ImageIO.read(new File(dir_f + fileName[in]));
				}
			} else {
				i[0] = ImageIO.read(new File(dir_f + fileName[0]));
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Image getI(int in) {
		return i[in];
	}

	public String[] getFileName() {
		return fileName;
	}

	public String getFileName(int i) {
		return fileName[i];
	}

	//	public boolean isGridPosType() {
	//		return isGridPosType;
	//	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setControl(MBController control) {
		this.control = control;
	}

	public MBController getControl() {
		return control;
	}

	//	public void setGridPosType(boolean isGridPosType) {
	//		this.isGridPosType = isGridPosType;
	//	}

	public void setFileNameMult(String[] fileName) {
		this.fileName = fileName;
	}

	//only sets the first one
	public void setFileNameSing(String fileName) {
		this.fileName[0] = fileName;
	}

}
