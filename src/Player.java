public class Player extends Char {
	boolean justTeleported = false;
	public Player(int x, int y, int tileSize, MBController c){
		super(x, y, 10, "player", tileSize, true, c);
		String[] strs = {"player_back.png", "player_left.png", "player_front.png", "player_right.png"};
		setFileNameMult(strs);
		constructImage();
	}
//	setPos(x, y);
//	setControl(c);
//	setTileSize(tileSize);
//	setHasFourImages(true);
//	String[] strs = {"player_back.png", "player_left.png", "player_front.png", "player_right.png"};
//	setFileNameMult(strs);
//	constructImage();
//	setType("player");
//	setSpeed(10);
	
	public void move(boolean[] b) {
		int newY = getY();
		int newX = getX();
		int oldY = newY;
		int oldX = newX;

		if (b[UP]) {
			setDir(UP);
			newY -= getSpeed();
		}
		if (b[LEFT]) {
			setDir(LEFT);
			newX -= getSpeed();
		}
		if (b[DOWN]) {
			setDir(DOWN);
			newY += getSpeed();
		}
		if (b[RIGHT]) {
			setDir(RIGHT);
			newX += getSpeed();
		}
		//		for (int i = 0; i < b.length; i++) {
		//			System.out.print(b[i] + " ");
		//		}
		//		System.out.println();

		//set the X first
		if(control.getMode() == 1){
			if (isMoveable(newX, newY) && !justTeleported) {
//				System.out.println(x + " " + oldX);
				setPos(newX, newY);
				//			System.out.println(new Point(newX, newY));
			} else if (isMoveable(newX, oldY) && !justTeleported) {
//				System.out.println(x + " " + oldX);
				setPos(newX, oldY);
			} else if (isMoveable(oldX, newY) && !justTeleported) {
//				System.out.println(x + " " + oldX);
				setPos(oldX, newY);
			}
			justTeleported = false;
//			System.out.println(getX());
		}

	}

	public void setJustTeleported(boolean justTeleported) {
		this.justTeleported = justTeleported;
	}

}
