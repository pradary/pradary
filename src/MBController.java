import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class MBController {
	MBModel model = null;
	MBView view = null;
	String mapDir = "maps/";
	//	int rows = 10;
	//	int cols = 10;
	Random ran = new Random();

	String map1 = "laundromat_interior";
	String map2 = "laundromat_exterior";
	String currMap = map1;
	
	int enemiesKilled = 0;
	
	public MBController(MBModel m, MBView v) {
		boolean usingMapWidth = false;
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int winL = (int) screenSize.getWidth();
		int winH = (int) screenSize.getHeight();
		int largeSize = 0;
		if (winL > winH) {
			largeSize = winL;
			usingMapWidth = true;
		} else {
			largeSize = winH;
			usingMapWidth = false;
		}
		//using largest size so the longer side of the tiles will fill the frames

		File file = new File(mapDir + currMap);
		//sample map testing
		try {
			//			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (UnsupportedLookAndFeelException e) {
		} catch (ClassNotFoundException e) {
		} catch (InstantiationException e) {
		} catch (IllegalAccessException e) {
		}
		boolean selectMap = false;
		if (selectMap) {
			JFileChooser fileP = new JFileChooser();
			int val = fileP.showOpenDialog(null);
			if (val == JFileChooser.APPROVE_OPTION) {
				file = fileP.getSelectedFile();
			}
		}

		BackgroundTile[][] map = readMap(file);
		m = new MBModel(this, largeSize, usingMapWidth, map);
		this.model = m;
		//		m.setMap(map);

		v = new MBView(this, largeSize, winL, winH);

		this.view = v;

	}

	private BackgroundTile[][] readMap(File f) {
		BackgroundTile[][] map = null;
		String fileName = "";
		String walkableString = "";
		boolean front = true;
		boolean walkable = false;

		try {
			@SuppressWarnings("resource")
			Scanner in = new Scanner(f);
			int row = 0, col = 0;

			//reads in the row and col
			int rows = in.nextInt();
			int cols = in.nextInt();
			map = new BackgroundTile[rows][cols];

			while (in.hasNext()) {
				if (front) {
					fileName = in.next();
					fileName = fileName.substring(1, fileName.length()); // gets rid of the [
					front = !front;
				} else {
					walkableString = in.next();
					walkableString = walkableString.substring(0, walkableString.length() - 1);
					front = !front;
					//custom walkable
					walkable = walkableString.equals("true");
					//					walkable = fileName.equals("ground.png");

					BackgroundTile nextBGTile = new BackgroundTile(fileName, walkable);
					map[row][col] = nextBGTile;

					col++;
					if (col >= cols) {
						col = 0;
						row++;
					}
				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return map;
	}

	//this moves the player
	//	public void move(boolean[] b) {
	//		model.getPlayer().move(b);
	//	}
	//	
	//	public void moveAI() {
	//		for (Entity ent : model.getEntities()) {
	//			if (ent.getType().equals("npc")) {
	//				((NPC) (ent)).move();
	//			}
	//		}
	//	}

	//moves both player and AI
	public void moveAll(boolean[] b) {
		ArrayList<Char> cs = model.getChars();
		for (int j = 0; j < cs.size(); j++) {
			Char c = cs.get(j);
			if (c.getType().equals("npc")) {
				((NPC) (c)).move();
			} else if (c.getType().equals("player")) {
				((Player) (c)).move(b);
			}
		}
		ArrayList<Bullet> bs = model.getBullets();
		for (int i = 0; i < bs.size(); i++) {
			boolean dec = bs.get(i).move();
			if (dec)
				i--;
		}
	}

	public void checkCollisions() {
		//can't use foreach when trying to remove bullet
		ArrayList<Bullet> bs = model.getBullets();
		ArrayList<Char> cs = model.getChars();
		for (int i = 0; i < bs.size(); i++) {
			//sometimes this breaks cuz remove?
			if(i < 0){
//				System.err.println("warning");
				break;
			}
			Bullet b = bs.get(i);
			for (int j = 0; j < cs.size(); j++) {
				Char c = cs.get(j);
				if (c.getType().equals("npc")) {
					//check if collided with this NPC
					Rectangle2D.Double bulRect = b.getCollision();
					Rectangle2D.Double charRect = c.getCollision();
					if (bulRect.intersects(charRect)) {
						//						System.out.println("hit!");
						//do something to char
						int dmg = b.getDamage();
						c.changeHealth(dmg * -1);
						if (c.getHealth() < 0) {
							cs.remove(c);
							j--;
							enemiesKilled++;
						}
						//remove the bullet
						bs.remove(b);
						i--;
					}
				}
			}
		}
	}
	public int getEnemiesKilled() {
		return enemiesKilled;
	}

	public void setEnemiesKilled(int enemiesKilled) {
		this.enemiesKilled = enemiesKilled;
	}
	int startNPCs = 3;
	public void startGame() {
		view.startGame();
		//		for (Char c : model.getChars()) {
		//			if (c.getType().equals("npc")) {
		//				((NPC) (c)).startT();
		//			}
		//		}
		for (int i = 0; i < startNPCs; i++) {
			int diff = ran.nextInt(2);
			newNPC(diff);
		}
	}

	public void changeMap() {
		int r = 0, c = 10;
		if(currMap.equals(map1)){
			currMap = map2;
			r = 5;
			c = 5;
//			System.out.println("ding " + r + " " + c);
		}else if(currMap.equals(map2)){
			currMap = map1;
			r = 8;
			c = 8;
//			System.out.println("dong " + r + " " + c);
		}
		model.getPlayer().setPos(c * getTileSize(), r * getTileSize());
		model.getPlayer().setJustTeleported(true);
		//load in the map and reinitiate stuff
		File file = new File(mapDir + currMap);
		BackgroundTile[][] map = readMap(file);
		model.setMap(map);
		int npcs = model.getChars().size() - 1;
		clearNPCs();
		for (int i = 0; i < npcs; i++) {
			int diff = ran.nextInt(2);
			newNPC(diff);
		}
		//really confused right now
		//why does set map to 2 does not update player pos but 1 does
//		System.out.println("before: " + model.getPlayer().getX());
	}

	//instead 10 10 have variable
	//player starts at 1 1
	//force repaint every move

	//generates a bullet the flies towards the mouse dir
	public void shoot(int x, int y) {
		//x y is the pos of the mouse
		int px = model.getPlayer().getX() + getTileSize()/2;
		int py = model.getPlayer().getY() + getTileSize()/2;
		x -= getTileSize() * .3;
		y -= getTileSize() * .3;
		double dx = x - px;
		double dy = y - py;
		double angle = Math.atan(dy / dx);
		if (dx < 0) {
			angle += Math.PI;
		}
		//		int angleDeg = (int) Math.toDegrees(angle);
		//		System.out.println(angleDeg + " " + dx + " " + dy);
		//angle is in rads
		model.shoot(px, py, angle);
	}

	public Image getBTImage(int r, int c) {
		return model.getBTImage(r, c);
	}

	public int getRows() {
		return model.getRows();
	}

	public int getCols() {
		return model.getCols();
	}

	public int getTileSize() {
		return model.getTileSize();
	}

	public int getMode() {
		return model.getMode();
	}

	public void updateView() {
		view.update();
	}

	public void setMode(int mode) {
		model.setMode(mode);
	}

	public Player getPlayer() {
		return model.getPlayer();
	}

	public void newNPC() {
		int diff = ran.nextInt(2);
		model.newNPC(diff);
	}

	public void newNPC(int diff) {
		model.newNPC(diff);
	}

	public boolean inBounds(int ro, int co) {
		return model.inBounds(ro, co);
	}

	public boolean isWalk(int ro, int co, boolean a) {
		return model.isWalk(ro, co, a);
	}

	public ArrayList<Char> getChars() {
		return model.getChars();
	}

	public ArrayList<Bullet> getBullets() {
		return model.getBullets();
	}

	public void clearNPCs() {
		model.clearNPCs();
	}
}